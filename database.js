const mysql = require('mysql');

const connection = mysql.createConnection({
    host     : process.env.DBEndpoint,
    user     : process.env.DBUsername,
    password : process.env.DBPassword,
    port     : process.env.DBPort
});

export const connectToDB = () => {
    connection.connect(function(err) {
        if (err) {
          console.error('Database connection failed: ' + err.stack);
          return;
        }
      
        console.log('Connected to database.');
    });
}

export const getAll = (connection, table) => {
    return new Promise((resolve, reject) => {
        connection.query({
            sql: 'SELECT * FROM `' + table + '`',
            timeout: 40000, // 40s
        },((error, results) => {
            if (error) {
                reject (error);
            }
            resolve (results);
        }));
    })
}

export const getOne = (connection, table, column, value) => {
    return new Promise((resolve, reject) => {
        connection.query({
            sql: 'SELECT * FROM `' + table + '` WHERE `' + column + '` = ?',
            timeout: 40000, // 40s
            values: [value]
        },((error, results) => {
            if (error) {
                reject (error);
            }
            resolve (results);
        }));
    })
}

export const createOne = (connection, table, object) => {
    return new Promise((resolve, reject) => {
        connection.query({
            sql: 'INSERT INTO `' + table + '` SET ?',
            timeout: 40000, // 40s
            values: object
        },((error, results) => {
            if (error) {
                reject (error);
            }
            resolve (results);
        }));
    })
}

const disconnectFromDB = (connection) => {
    connection.end();
}



